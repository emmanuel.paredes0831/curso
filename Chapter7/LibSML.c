#include "LibSML.h"
void MemoryView(float *const MemoryView,unsigned char Size){
    printf("\t Memory View \n");
    printf("\t 0\t 1\t 2\t 3\t 4\t 5\t 6\t 7\t 8\t 9\t 10\t 11\t 12\t 13\t 14\t 15\t 16\t 17\t 18\t 19\t 20\t 21\t 22\t 23\t 24\n");
    for (unsigned short i = 0; i < 1000; i++){
        if(i%25==0){
            printf("\n");
            printf("%d",i);
        }
        MemoryView[i] >= 0 ? printf("\t +%04.2f",MemoryView[i]) : printf("\t %04.2f",MemoryView[i]);
        
    }
    printf("\n");
    PAUSE;
}
void Help(void){
    printf("Ayuda\nSe mostraran los comandos disponibles:\n");
    printf("Comando\t\t Digito\n\n");

    printf(" READ\t\t 10\n WRITE\t\t 11\n LOAD\t\t 20\n STORE\t\t 21\n ADD\t\t 30\n SUBTRACT\t 31\n DIVIDE\t\t 32\n");
    printf(" MULTIPLY\t 33\n BRANCH\t\t 40\n BRANCHNEG\t 41\n BRANCHZERO\t 42\n HALT\t\t 43\n");
    printf(" HELP\t\t 50\n HISTORY\t 51\n MEMORYVIEW\t 52\n");
    PAUSE;
}

void History(float *const MemoryHistory,unsigned short *const Register_Accumulator){
    printf("History of memory assigment\n");
    if(*Register_Accumulator != 0 && *Register_Accumulator > 0){
        //printf("\t 0\t 1\t 2\t 3\t 4\t 5\t 6\t 7\t 8\t 9\n");
        for (unsigned char i = 0; i < *Register_Accumulator; i++){
            //if(i%10 == 0){
            //    printf("\n");
            //    printf("%d,",i);
            //}
            printf("Orden: %02d\t Comando: %04ld\n",i,MemoryHistory[i]);
        }
        
    }
    else{
        printf("No se ha realizado ninguna asignacion de memoria\n");
    }
    
}
unsigned char RegisterComando(unsigned long *Comando,unsigned char *OperatioCode, unsigned char *Operando){
    if(*Comando <100){
        *Comando *=100;
    }
    *OperatioCode =(unsigned char) *Comando/100;
    *Operando = (unsigned char)*Comando%100;
    if(*OperatioCode == HALT ){
        return 0;
    }
    else{
        return 1;
    }
    
}
void ComandSeletion(signed long *ArrayMemory,unsigned char *const OperatioCode, unsigned char *const Operando,unsigned char *const RegisterAccumulator){
    switch(*OperatioCode){
        case READ:{
            signed long A;
            printf("INtroduce el numero\n");
            scanf("%ld",&A);
            ArrayMemory[*Operando] = A;
            break;
        }
        case WRITE:{
            //printf("WRITE = %d\n",WRITE);
            printf("%ld",ArrayMemory[*Operando]);
            break;
        }
        case LOAD:{
            
            break;
        }
        case STORE:{
            printf("STORE = %d\n",STORE);
            break;
        }
        case ADD:{
            printf("ADD = %d\n",ADD);
            break;
        }
        case SUBTRACT:{
            printf("SUBTRACT = %d\n",SUBTRACT);
            break;
        }
        case DIVIDE:{
            printf("DIVIDE = %d\n",DIVIDE);
            break;
        }
        case MULTIPLY:{
            printf("MULTIPLY = %d\n",MULTIPLY);
            break;
        }
        case BRANCH:{
            printf("BRANCH = %d\n",BRANCH);
            break;
        }
        case BRANCHNEG:{
            printf("BRANCHNEG = %d\n",BRANCHNEG);
            break;
        }
        case BRANCHZERO:{
            printf("BRANCHZERO = %d\n",BRANCHZERO);
            break;
        }
        case HELP:{
            printf("HELP = %d\n",HELP);
            Help();
            PAUSE;
            break;
        }
        case HISTORY:{
            printf("HISTORY = %d\n",HISTORY);
            //History(ArrayMemory,RegisterAccumulator);
            
            break;
        }
        case MEMORYVIEW:{
            printf("MEMORIVIEW = %d\n",MEMORYVIEW);
            //MemoryView(ArrayMemory,SIZE_A);
            PAUSE;
            break;
        }
        case HALT:{
            printf("HALT = %d\n",HALT);
            break;
        }
    }

}
_Bool CorrectComand(unsigned char *const OperationCode){
    switch(*OperationCode){
        case READ:{
            return 1;
            break;
        }
        case WRITE:{
            return 1;
            break;
        }
        case LOAD:{
            return 1;
            break;
        }
        case STORE:{
            return 1;
            break;
        }
        case ADD:{
            return 1;
            break;
        }
        case SUBTRACT:{
            return 1;
            break;
        }
        case DIVIDE:{
            return 1;
            break;
        }
        case MULTIPLY:{
            return 1;
            break;
        }
        case BRANCH:{
            return 1;
            break;
        }
        case BRANCHNEG:{
            return 1;
            break;
        }
        case BRANCHZERO:{
            return 1;
            break;
        }
        case HELP:{
            return 1;

            break;
        }
        case HISTORY:{
            return 1;
            
            break;
        }
        case MEMORYVIEW:{
            return 1;
            break;
        }
        case HALT:{
            return 1;
            break;
        }
        default:{
            return 0;
            break;
        }
    }
}
