#include "operations.h"



void read(float *memory, unsigned char operand, unsigned short *instruction_counter){

    float word;

    printf("? ");
    scanf(" %f", &word);
    *instruction_counter += 1;//increases the instrcution counter to the next spot in memory


    memory[operand] = word;
}


void write(float *memory, unsigned char operand, unsigned short *instruction_counter){

    printf(":%f\n", memory[operand]);
    *instruction_counter += 1;//increases the instrcution counter to the next spot in memory

}


void load(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

     *accumulator = memory[operand];
     *instruction_counter += 1;
}


void store(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    memory[operand] = *accumulator;
    *instruction_counter += 1;
}


void add(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    *accumulator += memory[operand];
    *instruction_counter += 1;
}


void subtract(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    *accumulator -= memory[operand];
    *instruction_counter += 1;
}


void multiply(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    *accumulator *= memory[operand];
    *instruction_counter += 1;
}


void divide(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    if(memory[operand] == 0){
        *instruction_counter = 10001;
    }
    else{
        *accumulator /= memory[operand];
        *instruction_counter += 1;
    }
    
}


void branch(unsigned char operand, unsigned short *instruction_counter){

    *instruction_counter = operand;
}


void branchNeg(unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    if(accumulator < 0){
        *instruction_counter = operand;
    }else{

        *instruction_counter += 1;
    }
}


void branchZerro(unsigned char operand, float *accumulator ,unsigned short *instruction_counter){

    if(accumulator == 0){
        *instruction_counter = operand;
    }else{

        *instruction_counter += 1;
    }
}


void Halt(unsigned short *instruction_counter){

    printf("\n***the simpletron program has finished execution succesfully***\n");
    *instruction_counter = DONE;

}

void Bienvenido(void){
    printf("*** Welcome to Simpletron! ***\n");
    printf("*** Please enter your program one instruction ***\n");
    printf("*** (or data word) at the time. I will type the ***\n");
    printf("*** location number and a question mark (?). ***\n");
    printf("*** You then type the word for that location. ***\n");
    printf("*** Type the sentinel -99999 to stop entering ***\n");
    printf("*** your program. ***\n");
    printf("\n");
}
void Remainder(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){
    if(memory[operand] == 0 || *accumulator == 0){
        *instruction_counter = 10002;
    }
    else{
        fmod(*accumulator,memory[operand]);
        // *accumulator =(int) *accumulator % memory[operand];
        *instruction_counter += 1;
    }
}
void exponentiation(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter){
    *accumulator = pow(*accumulator,memory[operand]);
    *instruction_counter += 1;
}

void newLine(void){
    CLEAR_SCREEN;
    printf("Nueva Linea\n");
}