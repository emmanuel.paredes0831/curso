#include "LibSML.h"
/*Functions for Comands*/
/***************************/
/***************************/
/**Read
reads a word from the terminal into a specific location in memory
**/
void read(float *memory, unsigned char operand, unsigned short *instruction_counter);
/**Write
writes a word from a specific location in memory to the terminal
**/
void write(float *memory, unsigned char operand, unsigned short *instruction_counter);
/**Load
 loads a word from a specific location in memory into the accumulator
**/
void load(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Store
stares the value in the accumulator into a specific location in memory
**/
void store(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Add
adds a word from a specific location in memory to the accumulator(result left in accumulator)
**/
void add(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Subtract
subtracts a word from a specific location in memory from the accumulator(result left in accumulator)
**/
void subtract(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Multiply
multplies a word from a specific location in memory with the accumulator(result left in accumulator)
**/
void multiply(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Divide
divides a word from a specific location in memory by the accumulator(result left in accumulator)
**/
void divide(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Branch
branches to a specific location in memory
**/
void branch(unsigned char operand, unsigned short *instruction_counter);
/**Branch Negitive
branches to a specific location in memory if the accumulator is negative
**/
void branchNeg(unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Branch Zerro
branches to a specific location in memory if the accumulator is zerro
**/
void branchZerro(unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Halt
displays message to terminal about program being finished
*/
void Halt(unsigned short *instruction_counter);
/***************************/
/***************************/
/*Muestra el comentario de inicio*/
void Bienvenido(void);
/*Remainder
The remainder operator is an integer operator that can be used only whit intenger operands
*/
void Remainder(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**/
void exponentiation(float *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**/
void newLine(void);