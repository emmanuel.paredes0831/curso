/*  SIMPLETRON MACHINE LANGUAGE (SML) 
    OPERATION CODE 
*/
/*Privates Includes*********/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <math.h>
/***************************/
/***************************/
//DEFINITIONS  Comandos
//Input/Output Operations: 
#define READ        10
#define WRITE       11
//Load/Store Operations:
#define LOAD        20
#define STORE       21
//ARITHMETIC Operaions:
#define ADD         30
#define SUBTRACT    31
#define DIVIDE      32
#define MULTIPLY    33
#define REMAINDER   34
#define EXPO        35
#define NEWLINE     36
//Transfer of Control Operations
#define BRANCH      40
#define BRANCHNEG   41
#define BRANCHZERO  42
#define HALT        43
/***************************/
#define HELP        50
#define HISTORY     51
#define MEMORYVIEW  52
/***************************/
/***************************/

/////////////////////////////
#define DONE        1000
/***************************/
//Privates Definitions
#define SIZE_A          100
#define CLEAR_Var       fflush(stdin)
#define CLEAR_SCREEN    system("clear")
#define PAUSE           getchar()
#define NCOMANDOS       15
/***************************/
/***************************/
/*Privates Declarations*/
_Bool result;
float PararelMemory[1000];

/***************************/
/***************************/
/*Privates Typedef*/

/***************************/
/***************************/
/*Private Fuctions*/

/*Muestra toda la memoria asignada, donde se pide dos parametros, el array de 
memoria y la longitud del mismo*/
void MemoryView(float *const MemoryView,unsigned char Size);
/*Muestra los comandos disponibles*/
void Help(void);
/*Nos muestra todos los comandos realizados*/
void History(float *const MemoryHistory,unsigned short *const Register_Accumulator);
/*Fucnion a la que ingresa el comando y se encarga de el direccionamiento de datos*/
unsigned char RegisterComando(unsigned long  *Comando,unsigned char *OperatioCode, unsigned char *Operando);
/**/
void ComandSeletion(signed long *ArrayMemory,unsigned char *const OperatioCode, unsigned char *const Operando,unsigned char *const RegisterAccumulator);
/*FUncion que se encarga de la vericacion del comando instroducido sea el correxto*/
_Bool CorrectComand(unsigned char *const OperationCode);
/*Comprobacion de espacio libre en el array*/
_Bool ComprobationsMemory(unsigned long *const ArrayMemory,unsigned char *const Operando);
/*COmprobacion de continuacion del simpletron*/
_Bool ContinueSML();
/***************************/
/***************************/

/***************************/
/***************************/