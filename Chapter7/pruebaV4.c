#include "LibSML.h"
#include "operations.h"
void cargaComands(float *arrayMemory,const unsigned short *const contador);
void executions(float *arrayMemory);
void dump(float *arrayMemory);

int main(void){
float memory[1000] ={0};
// signed long *parallelMemory;
float memoryF[1000] = {0};

unsigned short contador = 0;

// parallelMemory = memory;
while(1){
     cargaComands(memory,&contador);//Carga los comando en el array
    //  parallelMemory = memory;
     PAUSE;
     executions(memory);//Ejecuta los comandos instroducidos
    dump(memoryF);
    break;
}
    return 0;
}

void cargaComands(float *arrayMemory,const unsigned short *const contador){
    float comand = 0000;
    unsigned char operatioCode = 00;
    unsigned char operand = 00;
    unsigned short counter = 0;
    unsigned char bucle;
    unsigned long comandoINt;
    Bienvenido();
    printf("%30s","Introduzca los comandos\n");
    do
    {
        do{
            printf("%d ?\t",counter);
            CLEAR_Var;
            scanf("%f",&comand);
            comandoINt = (unsigned long)comand;
            RegisterComando(&comandoINt,&operatioCode,&operand);
            if (comand != DONE){
                if(CorrectComand(&operatioCode)){
                arrayMemory[counter] = comand;
                counter ++;
                bucle = 0;
                }
                else{
                    printf("Comando inexistente\n Vuelva a interntarlo\n");
                    bucle = 1;
                }
            }
            else
            {
                counter = DONE;
            }
        }while(bucle);
    } while (counter < 1000);
    printf("*** Program loading completed ***\n");
    printf("*** Program execution begins ***\n");
}

void executions(float *arrayMemory){
    float accumulator = 0000;
    unsigned short Counter = 00;
    float comand = 0000;
    unsigned char operatioCode = 00;
    unsigned char operand = 00;
    unsigned long comandoINt;
    CLEAR_SCREEN;
    MemoryView(arrayMemory,SIZE_A);
    while(Counter< SIZE_A){
        comand = arrayMemory[Counter];
        comandoINt = (unsigned long)comand;
        RegisterComando(&comandoINt,&operatioCode,&operand);
        switch(operatioCode){
            case READ:{
                read(arrayMemory,operand,&Counter);
                break;
            }
            case WRITE:{
                write(arrayMemory,operand,&Counter);
                break;
            }
            case LOAD:{
                load(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case STORE:{
                store(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case ADD:{
                add(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case SUBTRACT:{
                subtract(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case DIVIDE:{
                divide(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case MULTIPLY:{
                multiply(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case BRANCH:{
                branch(operand,&Counter);
                break;
            }
            case BRANCHNEG:{
                branchNeg(operand,&accumulator,&Counter);
                break;
            }
            case BRANCHZERO:{
                branchZerro(operand,&accumulator,&Counter);
                break;
            }
            case HELP:{
                Help();

                break;
            }
            case HISTORY:{
                History(arrayMemory,&Counter);
                
                break;
            }
            case MEMORYVIEW:{
                MemoryView(arrayMemory,SIZE_A);
                break;
            }
            case HALT:{
                Halt(&Counter);
                Counter = DONE;
                break;
            }
            default:{
                printf("Comando desconocido, error!\nDesea continuar 1 = Si \t 0 = No");
                int i;
                CLEAR_Var;
                scanf("%d",&i);
                if(i <= 0){
                    Counter = DONE;
                } 
                break;
            }
           
        }
    }
    if(Counter == 10001){
        printf("\n*** Attem to divide by zero***\n");
        printf("\n***the simpletron execution abnormally terminated***\n\n\n");
    }
    else if(Counter == 10002){
        printf("\n*** Attem to Remainder by zero***\n");
        printf("\n***the simpletron execution abnormally terminated***\n\n\n");
    }
}
void dump(float *arrayMemory){
    MemoryView(arrayMemory,SIZE_A);
}