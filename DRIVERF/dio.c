#include "dio.h"
#include <math.h>

void dio_configPort(port_t *port, uint8_t config){
    /*uint8_t desplazar =1 << 7 ;
    for (uint8_t i = 0; i < 8; i++){
        port->TRIS[i] = config & desplazar ? 1 : 0;
    }*/
    //scanf("%hhi",&config);
    if(config != 0){
        port->TRIS = 255;
    }
    else{
        port->TRIS = 0;
    }
    
    
}

void dio_configPin( port_t *port, uint8_t pin, uint8_t config ){

    if(config != 0){
        port->TRIS = Set_Bit(port->TRIS,pin);
    }else{
        port->TRIS = Reset_Bit(port->TRIS,pin);
    }
    
}

void dio_writePort( port_t *port, uint8_t value ){//va en out
    if(port->TRIS == 0){
        fflush(stdin);
        //scanf("%3hhi",&value);
        port->OUT = value;
    }
}

uint8_t dio_readPort( port_t *port ){//lee va en in 
    if(port->TRIS == 255){
        fflush(stdin);
        scanf("%4hhi",&port->IN);
        return port->IN;
    }
    return 0;
}

void dio_setPin( port_t *port, uint8_t pin ){
    if(!bitcheck(port->TRIS,pin)){
        port->OUT = Set_Bit(port->OUT,pin);
    }
}

void dio_resetPin( port_t *port, uint8_t pin ){
    if(!bitcheck(port->TRIS,pin)){
        port->OUT = Reset_Bit(port->OUT,pin);
    }
}

void dio_togglePin( port_t *port, uint8_t pin ){
    //static uint8_t j=0;
    if(!bitcheck(port->TRIS,pin)){
        port->OUT = Toogle_BIT(port->OUT,pin);
    }
}

void dio_writePin( port_t *port, uint8_t pin, uint8_t value ){
    if(!bitcheck(port->TRIS,pin)){
        if(value != 0){
            //port->OUT = 1;
            port->OUT = Set_Bit(port->OUT,pin);
        }
        else{
            //port->OUT = 0;
            port->OUT = Reset_Bit(port->OUT,pin);
        }
        
    }
}

uint8_t dio_readPin( port_t *port, uint8_t pin ){
    if(bitcheck(port->TRIS,pin)){
        port->IN =  Set_Bit(port->IN,pin);
        return port->IN;
    }else{
        return 0;
    }
    
    
}

void dio_displayTest( void ){
    printf("Informacion en el registro PORTA.TRIS: ");
    verPuerto(&PORTA);
    printf("Informacion en el registro PORTB.TRIS: ");
    verPuerto(&PORTB);
    printf("Informacion en el registro PORTC.TRIS: ");
    verPuerto(&PORTC);
    printf("Informacion en el registro PORTD.TRIS: ");
    verPuerto(&PORTD);
    printf("Informacion en el registro PORTE.TRIS: ");
    verPuerto(&PORTE);
    printf("Informacion en el registro PORTF.TRIS: ");
    verPuerto(&PORTF);

    printf("\n");
    printf("Informacion en el registro PORTA.OUT: ");
    verPuertoOUT(&PORTA);
    printf("Informacion en el registro PORTB.OUT: ");
    verPuertoOUT(&PORTB);
    printf("Informacion en el registro PORTC.OUT: ");
    verPuertoOUT(&PORTC);
    printf("Informacion en el registro PORTD.OUT: ");
    verPuertoOUT(&PORTD);
    printf("Informacion en el registro PORTE.OUT: ");
    verPuertoOUT(&PORTE);
    printf("Informacion en el registro PORTF.OUT: ");
    verPuertoOUT(&PORTF);

    printf("\n");
    printf("Informacion en el registro PORTA.IN: ");
    verPuertoIN(&PORTA);
    printf("Informacion en el registro PORTB.IN: ");
    verPuertoIN(&PORTB);
    printf("Informacion en el registro PORTC.IN: ");
    verPuertoIN(&PORTC);
    printf("Informacion en el registro PORTD.IN: ");
    verPuertoIN(&PORTD);
    printf("Informacion en el registro PORTE.IN: ");
    verPuertoIN(&PORTE);
    printf("Informacion en el registro PORTF.IN: ");
    verPuertoIN(&PORTF);

}