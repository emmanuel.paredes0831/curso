#include "dio.h"
void parpadeo(void);
void suma(void);
void rotaBit(void);
void entradaYsalida(void);
void valorBit_a_Bit(void);
void rotarValores(void);

int main(void){
 
    //parpadeo();
    //suma();
    //rotaBit();
    //entradaYsalida();
    //valorBit_a_Bit();
    rotarValores();
    //dio_displayTest();
    return 0;
}

void parpadeo(void){
    dio_configPort(&PORTF,0);
    for(uint8_t i = 0; i<10; i++){
        dio_togglePin(&PORTF,DIO_PIN_6);
        sleep(1);        
        dio_togglePin(&PORTF,DIO_PIN_0);
        verPuertoOUT(&PORTF);
    }
}

void suma(void){
    uint16_t Puerto1 = 0;
    uint16_t Puerto2 = 0;
    uint16_t resultado;
    dio_configPort(&PORTB,1);
    dio_configPort(&PORTD,1);
    dio_configPort(&PORTA,0);

    Puerto1 = dio_readPort(&PORTB);
    Puerto2 = dio_readPort(&PORTD);

    resultado = Puerto1 + Puerto2;
    printf("Resultado = %d\n",resultado );
    if(resultado >255){
        dio_configPin(&PORTC,DIO_PIN_0,0);
        dio_setPin(&PORTC,DIO_PIN_0);
        resultado <<=1;
        resultado = (uint8_t)resultado;
        resultado >>=1;
        dio_writePort(&PORTA,(uint8_t)resultado);
    }
    else{
        dio_writePort(&PORTA,(uint8_t)resultado);
    }

}

void rotaBit(void){
    dio_configPort(&PORTF,0);
    uint8_t cuneta = 0;
    do{
        for (uint8_t i = 0; i < 9; i++){
            dio_setPin(&PORTF,i);
            if(i>= 1){
                dio_resetPin(&PORTF,i-1);
            }
            verPuertoOUT(&PORTF);
        }
        cuneta++;
    }while(cuneta < 3);
    
}

void entradaYsalida(void){
    dio_configPort(&PORTC,1);
    dio_configPin(&PORTC,DIO_PIN_0,0);
    dio_configPin(&PORTC,DIO_PIN_5,0);
    dio_configPin(&PORTC,DIO_PIN_7,0);

    for (uint8_t i = 0; i < 8; i++){
        dio_setPin(&PORTC,i);    
    } 
    for (uint8_t j = 0; j < 8; j++){
       dio_readPin(&PORTC,j);    
    }
    printf("\n");
}

void valorBit_a_Bit(void){
    dio_configPort(&PORTE,0);
    dio_writePort(&PORTE,15);

    for (uint8_t i = 8; i >0; i--){
        printf("Valor del puerto en el pin %d :  ",i-1);
        if(comprobacion(&PORTE,i-1)){
            printf("1\n");
        }
        else
        {
            printf("0\n");
        }
        
    }
    printf("\n");
    
}

void rotarValores(void){
    uint8_t lectura = 0;
    uint8_t shift = 0;
    dio_configPort(&PORTA,1);
    dio_configPort(&PORTB,1);
    dio_configPort(&PORTC,0);


    lectura = dio_readPort(&PORTA);
    verPuertoIN(&PORTA);

    shift = dio_readPort(&PORTB);
    verPuertoIN(&PORTB);

    lectura >>=shift; 
    dio_writePort(&PORTC,lectura);
    verPuertoOUT(&PORTC);
}