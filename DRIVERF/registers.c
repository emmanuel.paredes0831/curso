#include "registers.h"
/*port_t PORTA;
port_t PORTB;
port_t PORTC;
port_t PORTD;
port_t PORTE;
port_t PORTF;  */
port_t PORTA = {0,0,0};
port_t PORTB = {0,0,0};
port_t PORTC = {0,0,0};
port_t PORTD = {0,0,0};
port_t PORTE = {0,0,0};
port_t PORTF = {0,0,0};    



void verPuerto(port_t *port){
    uint8_t desplazar =1 << 7 ;
    printf(" Dec:%d\t    Hex:%x\t    Bin:",port->TRIS,port->TRIS);
    for (uint8_t i = 0; i < 8; i++){
        putchar(port->TRIS & desplazar ? '1' : '0');
        port->TRIS <<= 1;
    }
    
    printf("\n");
}

void verPuertoOUT(port_t *port){
    uint8_t desplazar =1 << 7 ;
    uint8_t cambio = port->OUT;
    printf(" Dec:%d\t    Hex:%x\t    Bin:",port->OUT,port->OUT);
    for (uint8_t i = 0; i < 8; i++){
        putchar(cambio & desplazar ? '1' : '0');
        cambio <<= 1;
    }
    printf("\n");
}

void verPuertoIN(port_t *port){
    uint8_t desplazar =1 << 7 ;
    uint8_t cambio = port->IN;
    printf(" Dec:%d\t    Hex:%x\t    Bin:",port->IN,port->IN);
    for (uint8_t i = 0; i < 8; i++){
        putchar(cambio & desplazar ? '1' : '0');
        cambio <<= 1;
    }
    printf("\n");
}
/*
void decToBin(port_t *port ,uint8_t value){
    uint8_t desplazar =1 << 7 ;
    for (uint8_t i = 0; i < 8; i++){
        port->OUT[i] = value & desplazar ? 1 : 0;
        putchar( value & desplazar ? '1' : '0');
        value <<=1;
        
    }
    printf("\n");
}*/

uint8_t comprobacion(port_t *port, uint8_t pin){
    if(bitcheck(port->OUT,pin)){
        return 1;
    }
    else
    {
        return 0;
    }
    
}