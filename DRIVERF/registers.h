/*Includes*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>

/**********/
/*Privates Typedefs*/

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;

typedef struct{
    uint8_t TRIS;
    uint8_t OUT;
    uint8_t IN;
}port_t;
 
extern port_t PORTA,PORTB,PORTC,PORTD,PORTE,PORTF;

/*Privates Definitios*/
#define Set_Bit(PORT,BIT)       (PORT|(1<<BIT)) 
#define Reset_Bit(PORT,BIT)     (PORT& ~(1<<BIT))
#define Toogle_BIT(PORT,BIT)    (PORT ^ (1<<BIT))
#define bitcheck(PORT,BIT)      (PORT &(1<<BIT))
/*********************/
/*********************

void Init(void);
void verPuerto(port_t *port);
void decToBin(port_t *port ,uint8_t value);
void verPuertoOUT(port_t *port);
void verPuertoIN(port_t *port);
*********************/

void verPuerto(port_t *port);
void verPuertoOUT(port_t *port);
void verPuertoIN(port_t *port);
uint8_t comprobacion(port_t *port,uint8_t pin);
