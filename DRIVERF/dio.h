/*Includes*/
#include "registers.h"
/**********/
/*Privates Definitios*/
#define DIO_PIN_0 0
#define DIO_PIN_1 1
#define DIO_PIN_2 2
#define DIO_PIN_3 3
#define DIO_PIN_4 4
#define DIO_PIN_5 5
#define DIO_PIN_6 6
#define DIO_PIN_7 7
/*********************/
/*Privates Prototype */

/*configura los 8 bits del puerto como salida o como entrada, 
donde un “1” indica configuración como salida y un “0” como entrada.
Cada bit del valor "config" representa un pin del puerto.*/
void dio_configPort(port_t *port, uint8_t config);

/*configura un bit del puerto como salida o como entrada,
donde un “1” indica configuración como salida y un “0” como entrada.*/
void dio_configPin( port_t *port, uint8_t pin, uint8_t config );

/*escribe un valor de 8 bits en el puerto indicado, 
la función sólo tendrá efecto si el puerto previamente está configurado como salida.*/
void dio_writePort( port_t *port, uint8_t value );

/*regresa los 8 bits del puerto indicado, 
la función sólo tendrá efecto si el puerto está configurado como entrada.*/
uint8_t dio_readPort( port_t *port );

/*escribe un “1” en el pin indicado del puerto indicado. 
la función sólo tendrá efecto si el puerto previamente está configurado como salida.*/
void dio_setPin( port_t *port, uint8_t pin );

/*escribe un “0” en el pin indicado del puerto indicado. 
la función sólo tendrá efecto si el puerto previamente está configurado como salida.*/
void dio_resetPin( port_t *port, uint8_t pin );

/*invierte el estado del pin indicado del puerto indicado. 
la función sólo tendrá efecto si el puerto previamente está configurado como salida.*/
void dio_togglePin( port_t *port, uint8_t pin );

/*escribe el valor indicado por “value” en el pin indicado del puerto indicado. 
la función sólo tendrá efecto si el puerto previamente está configurado como salida.*/
void dio_writePin( port_t *port, uint8_t pin, uint8_t value );

/*regresa el estado del pin indicado del puerto indicado. 
la función sólo tendrá efecto si el puerto está configurado como entrada.*/
uint8_t dio_readPin( port_t *port, uint8_t pin );

/*despliega los valores que existen en cada uno de los registros de todos los puertos en un 
formato amigable y visual que permita corroborar el correcto funcionamiento del driver.*/
void dio_displayTest( void );