#include <stdio.h>
#define SIZE 5
int main(void){
    int n[SIZE];
    for (size_t i = 0; i < SIZE; i++){
        n[i] = 2*2*i;
    }
    printf("%s%13s\n", "Element", "Value");

    for (size_t j = 0;j<SIZE ; ++j) {
         printf("%7u%13d\n", j, n[j]);
    }
    return 0;
}