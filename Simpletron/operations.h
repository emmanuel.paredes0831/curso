#include "LibSML.h"
/*Functions for Comands*/
/***************************/
/***************************/
/**Read
reads a word from the terminal into a specific location in memory
**/
void read(signed long *memory, unsigned char operand, unsigned short *instruction_counter);
/**Write
writes a word from a specific location in memory to the terminal
**/
void write(signed long *memory, unsigned char operand, unsigned short *instruction_counter);
/**Load
 loads a word from a specific location in memory into the accumulator
**/
void load(signed long *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Store
stares the value in the accumulator into a specific location in memory
**/
void store(signed long *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Add
adds a word from a specific location in memory to the accumulator(result left in accumulator)
**/
void add(signed long *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Subtract
subtracts a word from a specific location in memory from the accumulator(result left in accumulator)
**/
void subtract(signed long *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Multiply
multplies a word from a specific location in memory with the accumulator(result left in accumulator)
**/
void multiply(signed long *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Divide
divides a word from a specific location in memory by the accumulator(result left in accumulator)
**/
void divide(signed long *memory, unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Branch
branches to a specific location in memory
**/
void branch(unsigned char operand, unsigned short *instruction_counter);
/**Branch Negitive
branches to a specific location in memory if the accumulator is negative
**/
void branchNeg(unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Branch Zerro
branches to a specific location in memory if the accumulator is zerro
**/
void branchZerro(unsigned char operand, float *accumulator ,unsigned short *instruction_counter);
/**Halt
displays message to terminal about program being finished
*/
void Halt(unsigned short *instruction_counter);
/***************************/
/***************************/
void Bienvenido(void);