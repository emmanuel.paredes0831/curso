#include "LibSML.h"
#include "operations.h"
void sum(signed long *arraMemory);
void average(signed long *arrayMemory);
void varios(signed long *arrayMemory);
int main(void){
    signed long memory[SIZE_A];
    void(*funcioones[3])(signed long *arraMemory)={sum,average,varios};
    int sel;
    while (1){
        printf("%50s\n","Que desea seleccionar");
        printf("0 = Sum \n1 = Promedio\n2 = Suma de varios elementos\n");
        do{
            CLEAR_Var;
            scanf("%d",&sel);
        } while (!(sel >= 0 && sel < 3));

        (*funcioones[sel])(memory);
        
    }
    
    return 0;
}

void sum(signed long *arraMemory){
    
    float accumulator = 0000;
    unsigned short Counter = 00;
    unsigned long comand = 0000;
    unsigned char operatioCode = 00;
    unsigned char operand = 00;
    CLEAR_SCREEN;

    printf("%140s\n","SIMPLETRON MACHINE LANGUAGE (SML) OPERATION CODE");
    for(unsigned char i = 0; i<2;i++){
        for(unsigned char j = 0; j < 230;j++){
            printf("*");
        }
        printf("\n");
    }
    printf("%129s\n","Bienvenido, Introduzca los comandos");
    while(Counter < SIZE_A){
        printf("%30s","Introduzca los comandos\t");
        CLEAR_Var;
        scanf("%ld",&comand);
        RegisterComando(&comand,&operatioCode,&operand);
        //PararelMemory[Counter] = comand;
        arraMemory[Counter] = comand;
        switch(operatioCode){
            case READ:{
                read(arraMemory,operand,&Counter);
                break;
            }
            case WRITE:{
                write(arraMemory,operand,&Counter);
                break;
            }
            case LOAD:{
                load(arraMemory,operand,&accumulator,&Counter);
                break;
            }
            case STORE:{
                store(arraMemory,operand,&accumulator,&Counter);
                break;
            }
            case ADD:{
                add(arraMemory,operand,&accumulator,&Counter);
                break;
            }
            case SUBTRACT:{
                subtract(arraMemory,operand,&accumulator,&Counter);
                break;
            }
            case DIVIDE:{
                divide(arraMemory,operand,&accumulator,&Counter);
                break;
            }
            case MULTIPLY:{
                multiply(arraMemory,operand,&accumulator,&Counter);
                break;
            }
            case BRANCH:{
                branch(operand,&Counter);
                break;
            }
            case BRANCHNEG:{
                branchNeg(operand,&accumulator,&Counter);
                break;
            }
            case BRANCHZERO:{
                branchZerro(operand,&accumulator,&Counter);
                break;
            }
            case HELP:{
                Help();

                break;
            }
            case HISTORY:{
                History(arraMemory,&Counter);
                
                break;
            }
            case MEMORYVIEW:{
                MemoryView(arraMemory,SIZE_A);
                break;
            }
            case HALT:{
                Halt(&Counter);
                Counter = DONE;
                break;
            }
            default:{
                printf("Comando desconocido, error!\nDesea continuar 1 = Si \t 0 = No");
                int i;
                CLEAR_Var;
                scanf("%d",&i);
                if(i <= 0){
                    Counter = DONE;
                } 
                break;
            }
           
        }
    }

}
void average(signed long *arrayMemory){
    printf("Promedio\n");
}
void varios(signed long *arrayMemory){
    printf("Varios numeros\n");
}