#include "LibSML.h"
#include "operations.h"
void cargaComands(signed long *arrayMemory,const unsigned short *const contador);
void executions(signed long *arrayMemory);
void dump(signed long *arrayMemory);

int main(void){
signed long memory[SIZE_A] ={0};
signed long *parallelMemory;

unsigned short contador = 0;

parallelMemory = memory;
while(1){
    cargaComands(memory,&contador);//Carga los comando en el array
    parallelMemory = memory;
    PAUSE;
    executions(parallelMemory);//Ejecuta los comandos instroducidos
    dump(parallelMemory);
    break;
}
    return 0;
}

void cargaComands(signed long *arrayMemory,const unsigned short *const contador){
    unsigned long comand = 0000;
    unsigned char operatioCode = 00;
    unsigned char operand = 00;
    unsigned short counter = 0;
    unsigned char bucle;
    Bienvenido();
    printf("%30s","Introduzca los comandos\n");
    do
    {
        do{
            printf("%d ?\t",counter);
            CLEAR_Var;
            scanf("%ld",&comand);
            RegisterComando(&comand,&operatioCode,&operand);
            if (comand != DONE){
                if(CorrectComand(&operatioCode)){
                arrayMemory[counter] = comand;
                counter ++;
                bucle = 0;
                }
                else{
                    printf("Comando inexistente\n Vuelva a interntarlo\n");
                    bucle = 1;
                }
            }
            else
            {
                counter = DONE;
            }
        }while(bucle);
    } while (counter < SIZE_A);
    printf("*** Program loading completed ***\n");
    printf("*** Program execution begins ***\n");
}

void executions(signed long *arrayMemory){
    float accumulator = 0000;
    unsigned short Counter = 00;
    unsigned long comand = 0000;
    unsigned char operatioCode = 00;
    unsigned char operand = 00;
    CLEAR_SCREEN;
    MemoryView(arrayMemory,SIZE_A);
    while(Counter< SIZE_A){
        comand = arrayMemory[Counter];
        RegisterComando(&comand,&operatioCode,&operand);
        switch(operatioCode){
            case READ:{
                read(arrayMemory,operand,&Counter);
                break;
            }
            case WRITE:{
                write(arrayMemory,operand,&Counter);
                break;
            }
            case LOAD:{
                load(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case STORE:{
                store(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case ADD:{
                add(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case SUBTRACT:{
                subtract(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case DIVIDE:{
                divide(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case MULTIPLY:{
                multiply(arrayMemory,operand,&accumulator,&Counter);
                break;
            }
            case BRANCH:{
                branch(operand,&Counter);
                break;
            }
            case BRANCHNEG:{
                branchNeg(operand,&accumulator,&Counter);
                break;
            }
            case BRANCHZERO:{
                branchZerro(operand,&accumulator,&Counter);
                break;
            }
            case HELP:{
                Help();

                break;
            }
            case HISTORY:{
                History(arrayMemory,&Counter);
                
                break;
            }
            case MEMORYVIEW:{
                MemoryView(arrayMemory,SIZE_A);
                break;
            }
            case HALT:{
                Halt(&Counter);
                Counter = DONE;
                break;
            }
            default:{
                printf("Comando desconocido, error!\nDesea continuar 1 = Si \t 0 = No");
                int i;
                CLEAR_Var;
                scanf("%d",&i);
                if(i <= 0){
                    Counter = DONE;
                } 
                break;
            }
           
        }
    }
    if(Counter == 10001){
        printf("\n*** Attem to divide by zero***\n");
        printf("\n***the simpletron execution abnormally terminated***\n\n\n");
    }
}
void dump(signed long *arrayMemory){
    MemoryView(arrayMemory,SIZE_A);
}